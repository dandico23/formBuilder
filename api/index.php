<?php

require '../vendor/autoload.php';

use Slim\Http\{Request,Response};
$app = new \Slim\App();


$app->post('/builComponents', function (Request $request, Response $response) {
    $stepComponents = $request->getParsedBody();

    $result = array();
    $qtdComponents = count($stepComponents['component_type']);

    for ($i=0; $i < $qtdComponents; $i++) { 
        $result[$i]['component_type'] = $stepComponents['component_type'][$i];
        $result[$i]['data_name'] = $stepComponents['data_name'][$i];
        $result[$i]['hint'] = $stepComponents['hint'][$i];
        $result[$i]['label'] = $stepComponents['label'][$i];
        $result[$i]['required'] = "false";
        $result[$i]['lenght_max'] = "100";
        $result[$i]['length_min'] = "3";
        $result[$i]['invalid_text'] = "";
        $result[$i]['default_value'] = "";
        $result[$i]['invalid_text'] = "";
        $result[$i]['group'] = false;
    }

    return json_encode($result);
});

$app->post('/BuildForm', function (Request $request, Response $response) {
    $texto = json_decode($request->getBody());
    $formulario = array();
    $formulario['form_name']    = $texto->form_name;
    $formulario['form_titulo']  = $texto->form_titulo;
    $formulario['info_form']    = $texto->info_form;
    $formulario['form_version'] = "1.0";
    $formulario['area']         = "";
    $formulario['classe']       = "";
    $formulario['sub_classe']   = "";
    $steps = $texto->steps;
    foreach ($steps as $key => $step) {
        $formulario['steps'][]  = $step;
    }
    $formCompleto = json_encode($formulario, JSON_UNESCAPED_UNICODE);

    return $formCompleto;
});

$app->run();
