<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Form Builder</title>
  <link rel="stylesheet" href="jquery-ui.css">
  <link rel="stylesheet" href="style.css">
  <style>
  ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 30px; border:20px solid #dad55e; width: 520px; min-height: 30px; }
  li { margin: 5px; padding: 5px; width: 500px; }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".sortable" ).sortable({
      revert: true
    });
    $( "#draggable, #draggable2" ).draggable({
      connectToSortable: ".sortable",
      helper: "clone",
      revert: "invalid"
    });

    $( "ul, li" ).disableSelection();
  } );
  </script>
</head>
<body>
 <div>Arraste o componente abaixo para o Step desejado. É necessário adicionar pelo menos um STEP.</div>
 <div>Numero de passos:
 <input type="text" id="steps" value="0" readonly><br>
 </div>
 <div>
    <label for="form_name">Form name</label>
    <input type="text" id="form_name" value=""><br>
    <label for="title_form">Titulo do formulário</label>
    <input type="text" id="title_form" value="" ><br>
    <label for="info_form">Descrição formulário</label>
    <input type="text" id="info_form" value="" ><br>
 </div>
  <div id="form">
    <ul>
      <li id="draggable" class="ui-state-highlight">Componente<br>Tipo: 
        <select name="component_type[]">
          <option value="text">Selecione uma opção</option> 
          <option value="audiorec">Audio</option>
          <option value="camera">Câmera</option>
          <option value="date">Data</option> 
          <option value="croqui">Croqui</option>
          <option value="geoloc">Geolocalização</option>
          <option value="ocr">OCR</option>
          <option value="periodo">Período</option>  
          <option value="scanner">Scanner</option>
          <option value="text">Texto</option> 
          <option value="veiculo">Veiculo</option> 
        </select>
        data_name:
        <input type="text" required='true'name="data_name[]"><br><br>
        Hint:
        <input type="text" required='true'name="hint[]">
        Label:
        <input type="text" required='true'name="label[]">
      </li>
    </ul>
  </div>
    Monte aqui seu formulário: 

  <button onclick="criaPasso()">Adicionar novo Step</button>
  <button onclick="enviaFormulario()">Enviar formulário</button>
</body>
<script>
    function criaPasso() {
      var qtdpassos =  document.getElementById("steps").value;
      var para = document.createElement("div");
      para.innerHTML = '<ul><li class="ui-state-componente">Step name:<input type="text" required="true" id="step_name_'+qtdpassos +'">Info step:<input type="text" required="true" id="info_step_'+qtdpassos +'"></li></ul><form id='+'form_'+ qtdpassos + ' action=""><ul id="sortable" class="sortable ui-sortable"></ul>';
      document.getElementById("form").appendChild(para);

      $( function() {
        $( ".sortable" ).sortable({
        revert: true
        });
      $( "#draggable, #draggable2" ).draggable({
        connectToSortable: ".sortable",
        helper: "clone",
        revert: "invalid"
      });
      $( "ul, li" ).disableSelection();
      });

      qtdpassos++;
      document.getElementById("steps").value = qtdpassos;
    }
 </script>
 <script>
function enviaFormulario(){
    //var dados = jQuery( '#form_0').serializeArray();
    var qtdpassos =  document.getElementById("steps").value;
    var formName  =  document.getElementById("form_name").value;
    var formTitle = document.getElementById("title_form").value;
    var formInfo  =  document.getElementById("info_form").value;
    var steps = [];
    var formulario = [];
    //definindo cabeçalho do formulário
    formulario['form_name']= formName;
    formulario['form_titulo']= formTitle;
    formulario['info_form']= formInfo;

    //lendo os steps
    for(i=0;i<qtdpassos;i++){

        var form = '#form_'+ i;
        var stepN  = 'step_name_' + i;
        var stepName = document.getElementById(stepN).value;
        var stepI  = 'info_step_' + i;
        var stepInfo = document.getElementById(stepI).value;
        steps[i] = {};
        steps[i]['step_name'] = document.getElementById(stepN).value;
        steps[i]['step_description'] = stepInfo; 
        var Dados = new FormData(document.querySelector(form));
        var componentes = [];
        
        $.ajax({
            async: false,
            url: "api/builComponents",
            data: Dados,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data){
                componentes[i] = data;
            }
        });
        steps[i]['componentes'] = JSON.parse(componentes[i]);

    }
    formulario['steps'] = Object.assign({}, steps);
    console.log(formulario);
    formCompleto = Object.assign({}, formulario);
    console.log(formCompleto);
    teste = JSON.stringify(formCompleto);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "api/BuildForm",
    "method": "POST",
      "headers": {
      "Content-Type": "application/json",
      "Accept": "*/*",
      "Cache-Control": "no-cache"
    },
    "processData": false,
    "data":teste
    
  }
  $.ajax(settings).done(function (response) {
    console.log(response);
  });
}
</script>
 
</html>